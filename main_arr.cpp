#include "Array.h"
#include <iostream>

using namespace std;


Array<int> foo (int a, int b, int c){
	Array<int> arr (3);
	arr[0] = a;
	arr[1] = b;
	arr[2] = c;
	return arr;
}

int main(){
	/*
	Array<int> a (3);
	cout<<a;
	
	a[0]=1;
	a[2]=3;
	a[1]=11;
	
	cout<<"a\n"<<a;
	
	Array<int> b=a;
	
	cout<<"b\n"<<b;
	
	Array<int> c (4);
	
	cout<<"c before\n"<<c;
	
	c = b;
	
	cout<<"c after\n"<<c;
	
	Array<int> d=foo(1,2,3);
	
	cout<<"d (move ctor)\n"<<d;
	
	a=foo(1,2,3);
	
	cout<<"a after move assignment\n"<< a;
	*/
	Pair<int> a[2];
	//Pair<int> a (1,2);
	a[0] = Pair<int>(1,1);
	a[1] = Pair<int>(0,4);
	cout<<a[0]<<a[1]<<a[1][1];
	return 0;
}
