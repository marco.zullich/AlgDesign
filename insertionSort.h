#include<iostream>

void insertionSort(int *A, int size, int begin=0){
	for(int j=1; j<size; j++){
		int k = A[j];
		int i = j-1;
		while (i>=0 and A[i]>k){
			A[i+1] = A[i];
			i = i-1;
		}
		A[i+1] = k;
	}
}

