#include<iostream>
#include<stdafx.h>

template<typename T>
class SquareMatrix{
	T* elem;
	size_t n;
	
	public:
	SquareMatrix(size_t sz) : n{sz}, elem{new T [sz*sz]} {}
	~SquareMatrix() {delete[] elem;}
	
	unsigned int size(){return n;}
	
	T* operator[](const int i) noexcept { return &elem[i * n]; }
};

int main(){
	SquareMatrix<int> s(5);
	
	//std::cout<<s[0];
}