#ifndef UMAP
#define UMAP
#include <iostream>
#include "Array.h"

//custom map for the Graph's sake - it contains a pointer to the Vertex (T*) and a value
template <typename K, typename V>
class UMapPtr{
	public:
		struct Node{
			K* key;
			V val;
			Node* next;
			Node(K* pointer, V value, Node* n = nullptr) : key{pointer}, val{value}, next{n} {}
			~Node() {
				key=nullptr; next=nullptr;
			}
			Node(Node& other) : key{other.key}, val{other.val}, next{other.next} {}
			Node(Node&& other) : key{other.key}, val{std::move(val)}, next{other.next} {
				other.p=nullptr; other.next=nullptr;
			}
		};		
		
		Node* head;
	private:
		unsigned int sz;
		
		
	public:
		UMapPtr(Node* h = nullptr) : head{h}, sz{0} {}
		UMapPtr(K* key, V val) : head{new Node(key, val)}, sz{1} {}
		UMapPtr(Array<K>& keys, Array<V>& vals) : UMapPtr() {
			if(keys.size()==vals.size()){
				if(keys.size()>0){
					for (int i=0;i<keys.size();++i){
						this->insert(&keys[i],vals[i]);
					}
				}else
					throw "Empty input arrays";
			}else
				throw "Keys and vals not the same size";
		}
		
		~UMapPtr()  {
			if(head!=nullptr){
				eraseNext(head);
			}
		}
		
		UMapPtr(UMapPtr&& other) : head{other.head}, sz{std::move(other.sz)}{
			other.head = nullptr;
			other.sz = 0;
		}
		
		void insert(K* pointer, V val) {
			if(head==nullptr){
				head=new Node (pointer, val);
			}else{
				Node* temp (head);
				while(temp->next!=nullptr)
					temp = temp->next;
				temp->next = (new Node(pointer, val));
			}
			sz++;
		}
		
		const unsigned int  size(){
			return sz;
		}
		
		V& operator [] (K& lookup){
			Node* temp (head);
			while(temp!=nullptr){
				if(*temp.key==lookup)
					return temp->val;
				temp = temp->next;
			}
			this->insert(lookup,0);
			return this->at(sz-1);
			
		}
		
		V& at(const unsigned int index){
			if(index>=size)
				throw "Index out of bounds";
			Node* temp (head);
			unsigned int i {0};
			while(i<index){
				temp = temp->next;
				++i;
			}
			return temp->val;
		}
		
		
		
	private:
		//used for destructor
		void eraseNext(Node* n){
			if(n->next!=nullptr){
				eraseNext(n->next);
			}
			n = nullptr;
			sz--;
		}
		

	
};

template <typename K, typename V>
class UMapPtr2{
	public:
		struct Node{
			K* key;
			V* val;
			Node* next;
			Node(K* pointer, V* value, Node* n = nullptr) : key{pointer}, val{value}, next{n} {}
			~Node() {
				key=nullptr; next=nullptr;
			}
			Node(Node& other) : key{other.key}, val{other.val}, next{other.next} {}
			Node(Node&& other) : key{other.key}, val{other.val}, next{other.next} {
				other.p=nullptr; other.next=nullptr; other.val=nullptr;
			}
		};		
		
		Node* head;
	private:
		unsigned int sz;
		
		
	public:
		UMapPtr2(Node* h = nullptr) : head{h}, sz{0} {}
		UMapPtr2(K* key, V* val) : head{new Node(key, val)}, sz{1} {}
		UMapPtr2(Array<K>& keys, Array<V>& vals) :UMapPtr2() {
			if(keys.size()==vals.size()){
				if(keys.size()>0){
					for(int i=0;i<keys.size();++i){
						this->insert(&keys[i],&vals[i]);
					}
				}else
					throw "Empty input arrays";
			}else
				throw "Keys and vals not the same size";
		}
		
		~UMapPtr2()  {
			if(head!=nullptr){
				eraseNext(head);
			}
		}
		
		UMapPtr2(UMapPtr2&& other) : head{other.head}, sz{std::move(other.sz)}{
			other.head = nullptr;
			other.sz = 0;
		}
		
		void insert(K* pointer, V* val) {
			if(head==nullptr){
				head=new Node (pointer, val);
			}else{
				Node* temp (head);
				while(temp->next!=nullptr)
					temp = temp->next;
				temp->next = (new Node(pointer, val));
			}
			sz++;
		}
		
		const unsigned int  size(){
			return sz;
		}
		
		V* operator [] (K& lookup){
			Node* temp (head);
			while(temp!=nullptr){
				if(*temp.key==lookup)
					return temp->val;
				temp = temp->next;
			}
			this->insert(lookup,nullptr);
			return this->at(sz-1);
			
		}
		
		V* at(const unsigned int index){
			if(index>=size)
				throw "Index out of bounds";
			Node* temp (head);
			unsigned int i {0};
			while(i<index){
				temp = temp->next;
				++i;
			}
			return temp->val;
		}
		
		
		
	private:
		//used for destructor
		void eraseNext(Node* n){
			if(n->next!=nullptr){
				eraseNext(n->next);
			}
			n = nullptr;
			sz--;
		}
		

	
};

template <typename K, typename V>
std::ostream& operator<<(std::ostream& os, const UMapPtr<K,V>& u) {
	typename UMapPtr<K,V>::Node * temp;
	temp = u.head;
	os << "{";
	while(temp!=nullptr){
		os << "[" <<*(temp->key) << "," << (temp->val) << "], ";
		temp=temp->next;
	}
	os << "\b\b};\n" ;
  return os;
}

template <typename K, typename V>
std::ostream& operator<<(std::ostream& os, const UMapPtr2<K,V>& u) {
	typename UMapPtr2<K,V>::Node * temp;
	temp = u.head;
	os << "{";
	while(temp!=nullptr){
		os << "[" <<*(temp->key) << "," << *(temp->val) << "], ";
		temp=temp->next;
	}
	os << "\b\b};\n" ;
  return os;
}


#endif

