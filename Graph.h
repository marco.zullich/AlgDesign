#ifndef GRAPH_H
#define GRAPH_H
#include "Array.h"
#include "SLList.h"
#include<iostream>

template <typename T>	
struct Node {
	T node;
	Node()  {}
	Node(T n) : node{n} {}
	Node(Node& other) = delete;
	Node operator=(Node& other) = delete;
	Node(Node&& other) : node{std::move(other.n)} {}
	Node operator=(Node&& other) {
		node = std::move(other.node);
	}
	
	bool operator==(const Node& other){
		return this->node==other.node;
	}
};

template <typename T>
struct Edge {
	Node<T>* first;
	Node<T>* second;
	Edge () : first{nullptr}, second{nullptr} {}
	Edge (Node<T>* f, Node<T>* s) : first{f}, second{s} {}
	
	//Edge (Node& f, Node& s) : Edge(NodeRef(f),NodeRef(s)) {}
	//Edge (T& f, T& s) : Edge(NodeRef(Node(f)),NodeRef(Node(s))) {}		
	
	Edge (Edge& other) : first{other.first}, second{other.second}{}
	
	Edge& operator=(Edge& other){
		first=other.first;
		second=other.second;
	}
	
	Edge (Edge&& other) : first{other.first}, second{other.second} {
		other.first=nullptr;
		other.second=nullptr;
	}
	
	Edge& operator=(Edge&& other){
		first=other.first;
		second=other.second;
		other.first=nullptr;
		other.second=nullptr;
	}
};




template <typename T>
class Graph {
	private:
		Array<Node<T>> nodes;
		Array<Edge<T>> edges;
		
	public:
		struct AdjList{
			Graph* parent;
			Array<PointerList<Node<T>>>* adj;
			
			AdjList(Graph* gph) : parent{gph} {
				adj = new Array<PointerList<Node<T>>> (gph->size());
				for(int i=0;i<parent->size();i++){
					adj->get(i).insert(&parent->getNode(i));
					//adj->operator[](i) = PointerList<Node<T>> (&parent->getNode(i));
					for(int j=0; j<parent->edgSize();j++){
						if(*parent->getEdge(j).first==parent->getNode(i))
							adj->get(i).insert(parent->getEdge(j).second);
					}
				}
			}
			const unsigned int size(){
				return parent->size();
			}
			PointerList<Node<T>>& operator[](const unsigned int i){return adj->get(i);}
			const PointerList<Node<T>>& find(const T t){
				for(int i=0;i<&parent->size();i++){
					if(parent->getNode(i)== Node<T>(t))
						return adj->get(i);
					
				}
				throw "No node found for input " + t;
			}
	
		};

		Graph(Array<Node<T>>& nd, Array<Edge<T>>& edg) : nodes{std::move(nd)}, edges{std::move(edg)} {}
		
		Graph(Graph<T>& other) :
			nodes{other.nodes},
			edges{other.edges}
			{}
		
		Graph(Graph<T>&& other) :
			nodes{std::move(other.nodes)},
			edges{std::move(other.edges)}
			{}
		
		
		Array<Node<T>> getNodes()  {return nodes;}
		Array<Edge<T>> getEdges()  {return edges;}
		
		const Array<Node<T>> getNodes() const {return nodes;}
		const Array<Edge<T>> getEdges() const {return edges;}
		
		Node<T>& getNode(unsigned int i)  {return nodes[i];}
		Edge<T>& getEdge(unsigned int i)  {return edges[i];}
		
		const Node<T>& getNode(unsigned int i) const {return nodes[i];}
		const Edge<T>& getEdge(unsigned int i) const {return edges[i];}
		
		const unsigned int size() const {return nodes.size();}
		const unsigned int edgSize() const {return edges.size();}
		
		AdjList makeAdjList(){
			return AdjList(this);
		}
		
		
	//private:
		
		
};



template <typename T>
std::ostream& operator<<(std::ostream& os, const Graph<T>& g) {
	os << "Nodes:\n";
	for(int i=0;i<g.size();i++){
		os << g.getNode(i) << ", ";
	}
		
	os << "\b\b\nEdges:\n";
	for(int i=0;i<g.edgSize();i++){
		os << g.getEdge(i) << ", ";
	}
	os<<"\b\b;";
  return os;
}

//template<typename T>
//void breathFirstVisit(Graph<T> g, Node<T> v){
//	
//}



template <typename T>
std::ostream& operator<<(std::ostream& os, const Node<T>& node) {
	os << "Node: " << node.node ;
  return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Edge<T>& edge) {
	os << "Edge: [" << edge.first->node << "-->" << edge.second->node << "]" ;
  return os;
}

//template <typename T>
//std::ostream& operator<<(std::ostream& os, const typename Graph<T>::AdjList& adj) {
//	os << "Adjacency list:\n" ;
//	for(int i=0;i<adj.size();i++){
//		os << adj[i];
//	}
//  return os;
//}



#endif


