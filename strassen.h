#include<iostream>
#include"SquareMatrx.h"

SquareMatrix<int> strassen(SquareMatrix<int>& A, SquareMatrix<int>& B, int beginRA = 0, int beginCA = 0, int beginRB = 0, int beginCB = 0, int len = 0) {
	/*
	Computes product of 2 matrices A and B using strassen algorithm.
	Product is computed only for indices of A from [beginRA, beginCA] to [beginRA+len, beginCA+len]
	-----------------""-------------------- B from [beginRB, beginCB] to [beginRB+len, beginCB+len]
	For sake of simplicity, range isn't checked, nor is checked if size(A)=size(B), even if it's given for granted.
	NB: len is immediately standardized to size of A, since it cannot be set as default to this value.
	*/
	if (len == 0) {
		len = A.size();
	}
	
	//next: enlarge A and B if they don't have a size equal to 2^n
	//intpart hosts the integer part of the log2, decpart, the decimal
	double intpart;
	double decpart {modf(std::log2(len),&intpart)};
	//I store the original length of the matrices in a separate variable - it'll be necessary before returning the final matrix.
	int orig_len=len;
	//If size!=2^n -> I have to enlarge the matrices binding rows and cols of 0s until a size equal to 2^n is reached.
	if (decpart!=0.0){
		A.addRowCol(0,pow(2,static_cast<int>(intpart)+1)-len);
		B.addRowCol(0,pow(2,static_cast<int>(intpart)+1)-len);
		len=pow(2,static_cast<int>(intpart)+1);
		std::cout<<"\nrescaled from:"<<orig_len<<" to:"<<len<<std::endl;
	}
			
	//Special case: if the size is just 1, I have a simple product of integers and I don't need to recursively call the fct. again
	if (len == 1) {
		SquareMatrix<int> C (1, A[beginRA][beginCA]*B[beginRB][beginCB]);
		//SquareMatrix<int> C=multiplyNaive(A, B, beginRA, beginCA, beginRB, beginCB, len);
						return C;
	}
	
	//###COMPUTING 10 INTERMEDIATE MATRICES###
	//S1=B12-B22
	SquareMatrix<int> S1 = subtract(B, B, beginRB, (beginCB ) + len / 2, (beginRB ) + len / 2, (beginCB ) + len / 2, len / 2);
		//S2=A11+A12
	SquareMatrix<int> S2 = add(A, A, beginRA, beginCA, beginRA, (beginCA ) + len / 2, len / 2);
		//S3=A21+A22
	SquareMatrix<int> S3 = add(A, A, (beginRA ) + len / 2, beginCA, (beginRA ) + len / 2, (beginCA ) + len / 2, len / 2);
		//S4=B21-B11
	SquareMatrix<int> S4 = subtract(B, B, (beginRB ) + len / 2, beginCB, beginRB, beginCB, len / 2);
		//S5=A11+A22
	SquareMatrix<int> S5 = add(A, A, beginRA, beginCA, (beginRA ) + len / 2, (beginCA ) + len / 2, len / 2);
		//S6=B11+B22
	SquareMatrix<int> S6 = add(B, B, beginRB, beginCB, (beginRB ) + len / 2, (beginCB ) + len / 2, len / 2);
		//S7=A12-A22
	SquareMatrix<int> S7 = subtract(A, A, beginRA, (beginCA ) + len / 2, (beginRA ) + len / 2, (beginCA ) + len / 2, len / 2);
		//S8=B21-B22  -- correction it's B21+B22
	SquareMatrix<int> S8 = add(B, B, (beginRB ) + len / 2, beginCB, (beginRB ) + len / 2, (beginCB ) + len / 2, len / 2);
		//S9=A11-A21
	SquareMatrix<int> S9 = subtract(A, A, beginRA, beginCA, (beginRA ) + len / 2, beginCA, len / 2);
		//S10=B11+B12
	SquareMatrix<int> S10 = add(B, B, beginRB, beginCB, beginRB, (beginCB ) + len / 2, len / 2);


	
	
	//###COMPUTE 7 MATRICES BY MULTIPLICATION###
	//The 7 matrix products are obtained by recursively calling the method on submatrices.
	//P1=A11*S1 - RESULT GOES TO S1 SINCE WE DON'T NEED IT ANYMORE
	S1 = strassen(A, S1, beginRA, beginCA, 0, 0, len / 2);
	//P2=S2*B22 -> S2
	S2 = strassen(S2, B, 0, 0, (beginRB ) + len / 2, (beginCB ) + len / 2, len / 2);
	//P3=S3*B11 -> S3
	S3 = strassen(S3, B, 0, 0, beginRB, beginCB, len / 2);
	//P4=A22*S4 -> S4
	S4 = strassen(A, S4, (beginRA ) + len / 2, (beginCA ) + len / 2, 0, 0, len / 2);
	//P5=S5*S6 -> S5
	S5 = strassen(S5, S6);
	//P6=S7*S8 -> S6
	S6 = strassen(S7, S8);
	//P7=S9*S10 -> S7
	S7 = strassen(S9, S10);
	
	
	
	//###COMPUTE 4 FINAL SUBMATRICES BY ADDITION AND SUBTRACTION
	//C11=S5+S4-S2+S6
	SquareMatrix<int> C11 = add(S5, S4);
	C11 = subtract(C11, S2);
	C11 = add(C11, S6);
	
	//C12=P1+P2
	SquareMatrix<int> C12 = add(S1, S2);
	
	//C21=P3+P4
	SquareMatrix<int> C21 = add(S3, S4);
	
	//C22=P5+P1-P3-P7
	SquareMatrix<int> C22 = add(S5, S1);
	C22 = subtract(C22, S3);
	C22 = subtract(C22, S7);


	//The final part is to compose the return matrix C
	SquareMatrix<int> C(len);
	for (unsigned int i = 0; i < C11.size(); i++) {
		for (unsigned int j = 0; j < C11.size(); j++) {
			C[i][j] = C11[i][j];
			C[C11.size() + i][j] = C21[i][j];
			C[i][C11.size() + j] = C12[i][j];
			C[C11.size() + i][C11.size() + j] = C22[i][j];
		}
	}
	
	//Before returning, I have to re-scale the initial matrices A, B down to their original size
	//(in case I modified it); also the return matrix need be re-scaled since it ought to be of the same size of A and B.
	if(orig_len<len){
		A.remRowCol(len-orig_len);
		B.remRowCol(len-orig_len);
		C.remRowCol(len-orig_len);
	}
	return C;
	
}