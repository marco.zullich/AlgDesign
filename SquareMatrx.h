#include<iostream>

//Own class built to store Square matrices.
//The elements are stored in a one dimensional array with size n^2
//Elements are accessed by calling operator [], returning a pointer to the first element of the row (-> i*n)
//By calling SquareMatrix[i][j] i get the j-th element of the raw; hence, the [i][j] element of the matrix.
//Methods addRowCol and remRowCol are useful to increase/decrease the size of the matrix by a given no. of rows/cols.

//NB RANGE CHECKING ISN'T IMPLEMENTED FOR SAKE OF SIMPLICITY

template<typename T>
class SquareMatrix {
	T* elem;
	unsigned int n;

public:
	SquareMatrix(unsigned int sz) : n{ sz }, elem{ new T[sz*sz] } {}
	SquareMatrix(unsigned int sz, int defVal) : n{ sz }, elem{ new T[sz*sz] } {
		for (int i = 0; i < sz*sz; i++)
			elem[i] = defVal;
	};
	~SquareMatrix() { delete[] elem; }
	
  SquareMatrix(const SquareMatrix& m)
      : n{m.n}, elem{new T[n]} {
    if (m.moved)
      throw "cannot construct a Matrix from a moved one\n";
    for (int i = 0; i < n; ++i)
      new (&elem[i]) T{m.elem[i]};
  }

  SquareMatrix& operator=(const SquareMatrix& m) {
    if (&m != this) {
      if (m.moved)
        throw "better not to copy from an already moved matrix\n";

      if (moved) {
        n = m.n;
        elem.reset(new T[n*n]);
        moved = false;
      }
      // check sizes
      for (int i = 0; i < n; ++i)
        elem[i] = m.elem[i];
    }
    return *this;
  }

  SquareMatrix(SquareMatrix&& m)
	:
    n{std::move(m.n)},
	elem { m.elem}  {
		m.moved = true;
		m.n = 0;
		m.elem = nullptr;
		
	
  }

  SquareMatrix& operator=(SquareMatrix&& m) {
    if (m.moved)
      throw "better not to copy from an already moved matrix\n";
    n = std::move(m.n);
    elem = m.elem;
    m.moved = true;
    m.n = 0;
	m.elem = nullptr;
    return *this;
}

	void addRowCol(T val, int times=1){
		n=n+times;
		T* elem_new {new T[n*n]};
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if(i<n-times & j<n-times){
					elem_new[i*n+j] = elem[i*(n-times)+j];
				}else{
					elem_new[i*n+j] = val;
				}
			}
		}
		elem = elem_new;
		elem_new = nullptr;
		delete[] elem_new;
	}
	
	void remRowCol(int times=1){
		n=n-times;
		T* elem_new {new T[n*n]};
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				elem_new[i*n+j] = elem[i*(n+times)+j];
			}
		}
		elem = elem_new;
		elem_new = nullptr;
		delete[] elem_new;
	}
	
	bool moved = false;

	unsigned int size() { return n; }

	T* operator[](const int i) { return &elem[i * n]; }
	
};

template <typename T>
std::ostream& operator<<(std::ostream& os, SquareMatrix<T>& m) {
  for (auto i = 0; i < m.size(); ++i) {
    for (auto j = 0; j < m.size(); ++j)
      os << m[i][j] << ";";
    os << std::endl;
  }
  return os;
}

//the following methods add, subtract and multiply(with the naive algorithm) two square matrices (supposedly of same size)

SquareMatrix<int> add(SquareMatrix<int>& A,  SquareMatrix<int>& B, int beginRA=0, int beginCA=0, int beginRB=0, int beginCB=0, int len=0) {
	if(len==0){
		len=A.size();
	}
	SquareMatrix<int> S(len);
	for (int i = 0; i <len; i++) {
		for (int j = 0; j < len; j++) {
			S[i][j] = A[beginRA+i][beginCA+j] + B[beginRB+i][beginCB+j];
		}
	}
	return S;
}

SquareMatrix<int> subtract(SquareMatrix<int>& A,  SquareMatrix<int>& B, int beginRA=0, int beginCA=0, int beginRB=0, int beginCB=0, int len=0) {
	if(len==0){
		len=A.size();
	}
	SquareMatrix<int> S(len);
	for (int i = 0; i <len; i++) {
		for (int j = 0; j < len; j++) {
			S[i][j] = A[beginRA+i][beginCA+j] - B[beginRB+i][beginCB+j];
		}
	}
	return S;
}


SquareMatrix<int> multiplyNaive(SquareMatrix<int>& A, SquareMatrix<int>& B, int beginRA = 0, int beginCA = 0, int beginRB = 0, int beginCB = 0, int len = 0) {
	//std::cout << "��CALL OF NAIVEMULT WITH PARAMS: " << beginRA << ";" << beginCA << ";" << beginRB << ";" << beginCB << ";" << len << "\n";
	if (len == 0) {
		len = A.size();
	}
	
	SquareMatrix<int> M(len, 0);
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < len; j++) {
			for (int k = 0; k < len; k++) {
				M[i][j] += A[beginRA + i][beginCA + k] * B[beginRB + k][beginCB + j];
			}
		}
	}
	return M;
}