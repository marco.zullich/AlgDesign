#ifndef ARRAY_H
#define ARRAY_H

#include<iostream>
#include<algorithm>

template <typename T>
class Array {
	protected:
		unsigned int n;
		T* elem;
		bool moved = false;
		
	public:
		//standard c'tor
		Array(unsigned int _size) : n{_size}, elem{new T[_size]} {}
		//destructor
		~Array() { delete[] elem;}
		
		const unsigned int size() const {return n;}
		const bool isMoved() const {return moved;}
		
		T& operator[](const unsigned int pos) {return elem[rangecheck(pos)];}
		const T& operator[](const unsigned int pos) const {return elem[rangecheck(pos)];}
		
		T& get(const unsigned int pos) {return elem[rangecheck(pos)];}
		const T& get(const unsigned int pos) const {return elem[rangecheck(pos)];}
		
		//copy c'tor
		Array(const Array<T>& o) : n{o.size()}, elem{new T[n]} {
			if(o.isMoved())
				throw "Can't construct from a moved array";
			for(int i=0; i<n; i++){
				elem[i] =  o[i];
			}	
		}
		
		//copy assignment
		Array& operator=(const Array<T>& o){
			if(&o != this){
				if(o.moved)
					throw "Can't copy from a moved array";
				if(moved){
					n = o.size();
					delete[] elem;
					elem = new T[n];
					moved = false;
				}
				//NB we copy only up to the min size of both matrices
				for(int i=0;i<std::min(n,o.size());i++){
					elem[i] = o[i];
				}
			}
			return *this;
		}
		
		//move c'tor
		Array(Array<T>&& o) : n{o.n}, elem{new T[n]} {
			elem = o.elem;
			o.n = 0;
			o.moved = true;
			o.elem = nullptr;
		}
		
		//move assignment
		Array& operator=(Array<T>&& o){
			if(o.isMoved())
				throw "Can't copy from a moved array";
			n = o.size();
			elem = o.elem;
			moved = false;
			o.n = 0;
			o.moved = true;
			o.elem = nullptr;
			
		}
	

		
	private:
		const unsigned int rangecheck(const unsigned int i) const {if(i>=n) throw "Array index out of bounds"; return i;}
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const Array<T>& arr) {
	os << "[";
	for (auto i = 0; i < arr.size(); ++i) {
		os <<  arr[i] << ", ";
	}
	os << "\b\b]" << std::endl;
  return os;
}

template <typename T>
class Pair : public Array<T> {
	public:
		Pair() : Array<T>(2) {}
		Pair(T First, T Second) : Array<T>(2) {this->elem[0]=First;this->elem[1]=Second;}
		
};
#endif 

