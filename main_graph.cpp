#include "Graph.h"
#include "Array.h"
#include "SLList.h"
#include "UMap.h"

#include<iostream>
#include<utility>

using namespace std;
int main(){
	//  GRAPHS
	//Node<int> n (5);
	//
	Array<Node<int>> nodes (4);
    
	nodes[0] = Node<int>(1);
	nodes[1] = Node<int>(2);
	nodes[2] = Node<int>(3);
	nodes[3] = Node<int>(4);
	
	Array<double>  dd(4);
	dd[0] = (1.7);
	dd[1] = (2.7);
	dd[2] = (3.7);
	dd[3] = (4.7);
	
	//
	//cout << nodes;
	//
	//
	//
	//Array<Edge<int>> edges (2);
	//edges[0] = Edge<int>(&nodes[0],&nodes[1]);
	//edges[1] = Edge<int>(&nodes[2],&nodes[3]);
	//
	//cout << edges;
	//
	//Graph<int> g (nodes,edges);
	//
	//cout << g;
	//
	//Graph<int>::AdjList adj = g.makeAdjList();
	//
	//for(int i=0;i<adj.size();++i)
	//	cout<<adj[i]<<endl<<endl;
	//
	//cout<<adj;
	
	UMapPtr<Node<int>,double> u (nodes,dd);
	cout<<u;
	
	UMapPtr2<Node<int>,Node<int>> u2 (nullptr);
	for(int i=0;i<nodes.size();++i)
		u2.insert(&nodes[i],&nodes[nodes.size()-i-1]);
	
	cout<<u2;
	return 0;
}

