#include<iostream>
#include<cmath>
#include"strassen.h"





int main() {

	SquareMatrix<int> A(4, 2);
	SquareMatrix<int> B(4, 3);
	A[0][0] = 0;
	A[0][1] = 3;
	A[2][2] = 7;
	A[2][1] = -1;
	B[1][2] = -5;
	B[2][1] = 2;
	B[3][3] = 7;
	B[0][3] = 1;
	SquareMatrix<int> C = multiplyNaive(A, B);
	SquareMatrix<int> D = strassen(A,B);

	std::cout << "A" << std::endl << A;
	std::cout << "B" << std::endl << B << std::endl;
//
	std::cout << "Normal multiplication" << std::endl;
	std::cout << C;
	std::cout<<"Strassen multiplication"<<std::endl;
	std::cout<<D;
	SquareMatrix<int> Z (5,2);
	SquareMatrix<int> Z1 (5,2);
	SquareMatrix<int> H = strassen(Z,Z1);
	SquareMatrix<int> O = multiplyNaive(Z,Z1);
	std::cout<<"Z\n"<<Z<<"\n";
	std::cout<<"Z1\n"<<Z1<<"\n";
	std::cout<<"O\n"<<O<<"\n";
	std::cout<<"Z*Z1\n"<<H<<"----\n";


}

