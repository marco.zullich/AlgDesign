#include <iostream>
#include <utility>
#include "insertionSort.h"


int selectPivot(int* A, int begin, int end){
	numBlocks = (end-begin+1)/5;
	medians = int[numBlocks];
	for (i=0;i<numBlocks;i++){
		cbegin = begin + 5*i;
		insertionSort(A,cbegin,cbegin+4);
		medians[i] = A[cbegin+2];
	}
	return select(medians,numOfBlocks/2,0,numOfBlocks-1);
}

std::pair<int,int> selectPartition(int* A, int begin, int end, int pivot){
	return std::make_pair(begin,end);
}

int select(int* A, int j, int begin, int end){
	/*
		A is array of integers
		j is the index of the element of the sorted array
		begin - end is the range of A to which we apply
		  this method
	*/
	
	if (end-begin+1 < 140){
		insertionSort(A,end);
		return A[j];
	}
	int pivot = selectPivot(A, begin, end);
	std::pair<int,int> K = selectPartition(A, begin, end, pivot);
	
	if(j<K.first){
		return select(A,j,begin,K.first-1);
	}
	if(j>K.second){
		return select(A,j,K.second+1,end);
	}
	return A[j];
	
}


