#ifndef SLLIST_H
#define SLLIST_H

#include<iostream>

template <class T>
class PointerList {
	public:
		struct Node{
			T* p;
			Node* next;
			Node(T* pointer, Node* n = nullptr) : p{pointer}, next{n} {}
			~Node() {
				p=nullptr; next=nullptr;
			}
			Node(Node& other) : p{other.p}, next{other.next} {}
			Node(Node&& other) : p{other.p}, next{other.next} {
				other.p=nullptr; other.next=nullptr;
			}
		};
		
		Node* head;
	private:
		unsigned int sz;
	
	public:
		PointerList(Node* h = nullptr) : head{h}, sz{0} {}
		PointerList(T* t) : head{new Node(t)}, sz{1} {}
		~PointerList()  {
			if(head!=nullptr){
				eraseNext(head);
			}
		}
		
		PointerList(PointerList&& other) : head{other.head}, sz{std::move(other.sz)}{
			other.head = nullptr;
			other.sz = 0;
		}
		
		void insert(T* pointer) {
			if(head==nullptr){
				head=new Node (pointer);
			}else{
				Node* temp (head);
				while(temp->next!=nullptr)
					temp = temp->next;
				temp->next = (new Node(pointer));
			}
			sz++;
		}
		
		const unsigned int  size(){
			return sz;
		}
		
		T& operator []  (const unsigned int index){
			if(head==nullptr)
				throw "Index out of bounds";
			int i=0;
			Node* temp (head);
			while(i<index){
				temp = temp->next;
				++i;
			}
			return *temp->p;
		}
		
		const unsigned int find(T lookup){
			int i = 0;
			Node* temp (head);
			while(temp!=nullptr){
				if(*temp->p == lookup)
					return i;
				temp=temp->next;
				++i;
			}
			return -1;
				
		}
		
		
		
	private:
		//used for destructor
		void eraseNext(Node* n){
			if(n->next!=nullptr){
				eraseNext(n->next);
			}
			n = nullptr;
			sz--;
		}
		

	
};

template <typename T>
std::ostream& operator<<(std::ostream& os, const PointerList<T>& l) {
	typename PointerList<T>::Node * temp;
	temp = l.head;
	os << "[";
	while(temp!=nullptr){
		os << *(temp->p) << ", ";
		temp=temp->next;
	}
	os << "\b\b];\n" ;
  return os;
}


#endif

